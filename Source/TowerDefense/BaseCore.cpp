// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCore.h"
#include "TDPlayer.h"
#include "Enemy.h"
#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
// Sets default values
ABaseCore::ABaseCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

int32 ABaseCore::GetHealth()
{
	return health;
}

// Called when the game starts or when spawned
void ABaseCore::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABaseCore::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* Enemy = Cast<AEnemy>(OtherActor))
	{
		ATDPlayer* player = Cast<ATDPlayer>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		player->ReduceHealth(1);
		player->CheckDead();
		OnCoreHit.Broadcast(this);
	}
}

// Called every frame
void ABaseCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

