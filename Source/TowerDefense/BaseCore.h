// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseCore.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCoreHitSignature, class ABaseCore*, Core);

UCLASS()
class TOWERDEFENSE_API ABaseCore : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(BlueprintAssignable)
	FCoreHitSignature OnCoreHit;
	
	// Sets default values for this actor's properties
	ABaseCore();
	
	int32 GetHealth();

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 health;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
