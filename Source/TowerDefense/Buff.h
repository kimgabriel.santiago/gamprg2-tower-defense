// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Buff.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuff : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuff();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float buffDuration;

	float elapsedTime;

	FTimerHandle BuffTimeHandler;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString buffName;

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float buffPow;

	UFUNCTION()
	void DeleteBuff();
	
	UFUNCTION()
	void RestartDuration();

	UPROPERTY()
	AActor* target;

	UFUNCTION()
		virtual void BuffEffectStart();

	UFUNCTION()
		virtual void BuffEffectEnd();
};
