// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffManagerComponent.h"
#include "Buff.h"


// Sets default values for this component's properties
UBuffManagerComponent::UBuffManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBuffManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBuffManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UBuffManagerComponent::DestroyAllBuffs()
{
	TArray<ABuff*> BuffListToDestroy;

	for (int i = 0; i < BuffList.Num(); i++)
	{
		BuffListToDestroy.Add(BuffList[i]);
	}
	if (BuffListToDestroy.Num() > 0)
	{
		for (int i = BuffListToDestroy.Num(); i > 0; i--)
		{
			BuffListToDestroy[i - 1]->DeleteBuff();
		}
	}
}

void UBuffManagerComponent::DestroySpecificBuff(FString buffName)
{
	TArray<ABuff*> BuffListToDestroy;

	for (int i = 0; i < BuffList.Num(); i++)
	{
		if (buffName == BuffList[i]->buffName)
		{
			BuffListToDestroy.Add(BuffList[i]);
		}
	}
	if (BuffListToDestroy.Num() > 0)
	{
		for (int i = BuffListToDestroy.Num(); i > 0; i--)
		{
			BuffListToDestroy[i - 1]->DeleteBuff();
		}
	}

}

bool UBuffManagerComponent::isBuffInManager(FString buffName)
{
	for (int i = 0; i < BuffList.Num(); i++)
	{
		if (buffName == BuffList[i]->buffName)
		{
			return true;
		}
	}
	return false;
}

ABuff* UBuffManagerComponent::GetBuffReference(FString buffName)
{
	for (int i = 0; i < BuffList.Num(); i++)
	{
		if (buffName == BuffList[i]->buffName)
		{
			return BuffList[i];
		}
	}
	return NULL;
}

void UBuffManagerComponent::SpawnBuff(TSubclassOf<class ABuff> buff, float nBuffPow)
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;

	FTransform spawnTransform;

	if (buff != NULL)
	{
		ABuff* spawnedBuff = GetWorld()->SpawnActor<ABuff>(buff, spawnTransform, spawnParams);
		spawnedBuff->target = this->GetOwner();
		spawnedBuff->buffPow = nBuffPow;
		spawnedBuff->BuffEffectStart();
		BuffList.Add(spawnedBuff);
	}
}

