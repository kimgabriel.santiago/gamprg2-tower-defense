// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffManagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UBuffManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuffManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class ABuff*> BuffList;

	UFUNCTION()
	void DestroyAllBuffs();

	UFUNCTION()
	void DestroySpecificBuff(FString buffName);

	UFUNCTION()
	bool isBuffInManager(FString buffName);

	UFUNCTION()
	ABuff* GetBuffReference(FString buffName);

	UFUNCTION()
	void SpawnBuff(TSubclassOf<class ABuff> buff, float nBuffPow);

		
};
