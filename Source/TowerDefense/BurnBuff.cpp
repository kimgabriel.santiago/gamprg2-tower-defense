// Fill out your copyright notice in the Description page of Project Settings.


#include "BurnBuff.h"
#include "Enemy.h"
#include "HealthComponent.h"

ABurnBuff::ABurnBuff()
{

}

void ABurnBuff::BeginPlay()
{
	Super::BeginPlay();
}

void ABurnBuff::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void ABurnBuff::BuffEffectStart()
{
	GetWorldTimerManager().SetTimer(BuffTimeHandler, this, &ABurnBuff::BuffEffectStart, 1, true);
	Cast<AEnemy>(target)->HealthComponent->ReduceHealth(buffPow);

}

void ABurnBuff::BuffEffectEnd()
{
	

}
