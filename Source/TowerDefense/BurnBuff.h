// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "BurnBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ABurnBuff : public ABuff
{
	GENERATED_BODY()
public :
	ABurnBuff();

protected:
	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;
	

	void BuffEffectStart() override;

	void BuffEffectEnd() override;

};
