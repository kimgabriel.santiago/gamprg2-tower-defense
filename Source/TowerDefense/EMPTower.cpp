// Fill out your copyright notice in the Description page of Project Settings.


#include "EMPTower.h"
#include "Enemy.h"
#include "BuffManagerComponent.h"
#include "SlowBuff.h"

AEMPTower::AEMPTower()
{

}
void AEMPTower::BeginPlay()
{
	Super::BeginPlay();
}

void AEMPTower::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AActor* target = Cast<AEnemy>(OtherActor))
	{
		AEnemy* tempEnemy = Cast<AEnemy>(target);

		if (!EnemiesInRange.Contains(target))
		{
			EnemiesInRange.Add(target);
			ApplySlowBuff(target);
		}

	}
}

void AEMPTower::OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AActor* target = Cast<AEnemy>(OtherActor))
	{
		
		if (EnemiesInRange.Contains(target))
		{
			RemoveSlowBuff(target);
			EnemiesInRange.Remove(target);
		}

	}
}

void AEMPTower::ApplySlowBuff(AActor* target)
{
	Cast<AEnemy>(target)->BuffManager->SpawnBuff(buffSlow, slowStrength);
}

void AEMPTower::RemoveSlowBuff(AActor* target)
{
	Cast<AEnemy>(target)->BuffManager->DestroySpecificBuff(buffSlow->GetDefaultObject<ABuff>()->buffName);
}
