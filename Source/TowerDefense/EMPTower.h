// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "EMPTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEMPTower : public ATowerBase
{
	GENERATED_BODY()
	
public:
	AEMPTower();
protected:
	virtual void BeginPlay() override;

	virtual void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
	
	
	UFUNCTION()
	void ApplySlowBuff(AActor* target);

	UFUNCTION()
	void RemoveSlowBuff(AActor* target);

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ABuff> buffSlow;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float slowStrength;

};
