// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Components/BoxComponent.h" 
#include "EnemyAIController.h"
#include "PathNode.h"
#include "Kismet/GameplayStatics.h"
#include "HealthComponent.h"
#include "BuffManagerComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);

	CollisionBoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	CollisionBoxComp->SetupAttachment(RootComponent);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	this->AddOwnedComponent(HealthComponent);

	BuffManager = CreateDefaultSubobject<UBuffManagerComponent>(TEXT("Buff Manager"));
	this->AddOwnedComponent(BuffManager);
	waypointIdx = 0;


}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	/*StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);*/

	SpawnDefaultController();

	MoveNextWaypoint();

	baseSpeed = GetCharacterMovement()->MaxWalkSpeed;
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::MoveNextWaypoint()
{
	
	if (Waypoints.Num() != NULL)
	{
		if (Waypoints.Num() > 0 && waypointIdx < Waypoints.Num())
		{
			Cast<AEnemyAIController>(GetController())->MoveToActor(Cast<APathNode>(Waypoints[waypointIdx]));
			waypointIdx++;
		}
		else if (waypointIdx >= Waypoints.Num())
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Last to Waypoint"));
		}
	}
}

void AEnemy::SetWaypoints(TArray<AActor*> newWaypoints)
{
	Waypoints = newWaypoints;
}

void AEnemy::ScaleHealth(int waveCount)
{
	float rate;
	rate = waveCount * 0.2;

	HealthComponent->AdjustHealth(rate);
}


void AEnemy::Die()
{
	OnEnemyDeath.Broadcast(); 
	Destroy();
} 

void AEnemy::CheckDead()
{
	if (HealthComponent->GetCurrentHP() <= 0)
	{
		OnEnemyKilled.Broadcast();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Dead Enemy"));
		Die();
	}
}

void AEnemy::ModifySpeed(float speed)
{
	GetCharacterMovement()->MaxWalkSpeed = speed;

	baseSpeed = GetCharacterMovement()->MaxWalkSpeed;
}



