// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeathEnemySignature);

UCLASS()
class TOWERDEFENSE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
	FDeathEnemySignature OnEnemyDeath;

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
	FDeathEnemySignature OnEnemyKilled;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UBoxComponent* CollisionBoxComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 waypointIdx;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite , Category = "Waypoint Targets")
	TArray<AActor*>Waypoints;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 attackPow;
public:	
	// Called every frame	
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void MoveNextWaypoint();

	void SetWaypoints(TArray<AActor*> newWaypoints);

	void ScaleHealth(int waveCount);

	UFUNCTION(BlueprintCallable)
	void Die();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBuffManagerComponent* BuffManager;

	UFUNCTION()
	void CheckDead();

	UFUNCTION()
	void ModifySpeed(float speed);

	float baseSpeed;


};
