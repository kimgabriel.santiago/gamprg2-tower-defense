// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserTower.h"
#include "Enemy.h"
#include "BuffManagerComponent.h"
#include "BurnBuff.h"

void ALaserTower::Fire()
{
	Super::Fire();

	if (ABuff* buff = currentEnemyTarget->BuffManager->GetBuffReference(BurnBuff->GetDefaultObject<ABuff>()->buffName))
	{
		if (buff != NULL)
		{
			buff->RestartDuration();
		}
	}
	else
	{
		currentEnemyTarget->BuffManager->SpawnBuff(BurnBuff, TowerDamage);
	}
	ResetTarget();
}

void ALaserTower::SetTarget()
{
	AEnemy* enemyWithNoBuff = NULL;
	AEnemy* closestEnemy = NULL;

	if (EnemiesInRange.Num() != NULL && EnemiesInRange.Num() > 0)
	{
		if (EnemiesInRange.Num() == 1)
		{
			closestEnemy = Cast<AEnemy>(EnemiesInRange[0]);

			if (closestEnemy->BuffManager->isBuffInManager(BurnBuff->GetDefaultObject<ABuff>()->buffName))
			{
				return;
			}

			enemyWithNoBuff = Cast<AEnemy>(EnemiesInRange[0]);
		}
		else
		{
			for (int i = 0; i < EnemiesInRange.Num() - 1; i++)
			{
				AEnemy* enemy1 = Cast<AEnemy>(EnemiesInRange[i]);
				AEnemy* enemy2 = Cast<AEnemy>(EnemiesInRange[i + 1]);

				if (GetDistanceTo(enemy1) < GetDistanceTo(enemy2))
				{
					closestEnemy = enemy1;
					if (!enemy1->BuffManager->isBuffInManager(BurnBuff->GetDefaultObject<ABuff>()->buffName))
					{
						enemyWithNoBuff = enemy1;
					}

				}
				else
				{
					closestEnemy = enemy2;
					if (!enemy2->BuffManager->isBuffInManager(BurnBuff->GetDefaultObject<ABuff>()->buffName))
					{
						enemyWithNoBuff = enemy2;
					}
				}
			}
		}

		if (enemyWithNoBuff != NULL)
		{
			currentEnemyTarget = enemyWithNoBuff;
		}
		else if (closestEnemy != NULL)
		{
			currentEnemyTarget = closestEnemy;
		}
	}
}

//void ALaserTower::StartFiring()
//{
//	if (!isFiring)
//	{
//		GetWorldTimerManager().SetTimer(FiringTimerHandle, this, &ALaserTower::Fire, fireRate, true);
//		isFiring = true;
//	}
//}
