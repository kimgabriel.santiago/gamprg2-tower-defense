// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SingleTargetTurrets.h"
#include "LaserTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ALaserTower : public ASingleTargetTurrets
{
	GENERATED_BODY()
private:

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ABuff> BurnBuff;

	void Fire() override;

	void SetTarget() override;

	//void StartFiring() override;


};
