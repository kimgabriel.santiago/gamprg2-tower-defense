// Fill out your copyright notice in the Description page of Project Settings.


#include "MissileProjectile.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"
#include "HealthComponent.h"

AMissileProjectile::AMissileProjectile()
{
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	SphereComponent->SetupAttachment(RootComponent);
}
void AMissileProjectile::BeginPlay()
{
	Super::BeginPlay();

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AMissileProjectile::OnActorOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &AMissileProjectile::OnProjectileOverlapEnd);

}

void AMissileProjectile::OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (!TargetsInMissileRange.Contains(target))
		{
			TargetsInMissileRange.Add(target);
		}
	}
}

void AMissileProjectile::OnProjectileOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (TargetsInMissileRange.Contains(target))
		{
			TargetsInMissileRange.Remove(target);
		}
	}
}

void AMissileProjectile::OnProjectileOverlapEffect(AActor* otherActor)
{
	for (int i = 0; i < TargetsInMissileRange.Num(); i++)
	{
		TargetsInMissileRange[i]->HealthComponent->ReduceHealth(damage);
	}
}

void AMissileProjectile::missileExplosion()
{

}
