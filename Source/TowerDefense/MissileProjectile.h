// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissileProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMissileProjectile : public AProjectile
{
	GENERATED_BODY()
public:
	AMissileProjectile();

protected:
	void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USphereComponent* SphereComponent;
	
	UPROPERTY(VisibleAnywhere)
	TArray <class AEnemy*> TargetsInMissileRange;

	UFUNCTION()
	void OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
	void OnProjectileOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void OnProjectileOverlapEffect(AActor* otherActor);

public:
	UFUNCTION()
	void missileExplosion();

};
