 // Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "Enemy.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/MovementComponent.h"
#include "Components/SphereComponent.h"
#include "HealthComponent.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	SetRootComponent(BaseMesh);



	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	AddOwnedComponent(ProjectileMovement);
	ProjectileMovement->UpdatedComponent = BaseMesh;

	lifeSpan = 1.5f;

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	DelayDestroy();
	BaseMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnOverlap);
	
}

void AProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	
}

void AProjectile::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("On hit Enemy Message"));
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("On hit Enemy Message"));
		
		enemy->HealthComponent->ReduceHealth(damage);
		enemy->CheckDead();
		DestroyBullet();
	}
}

void AProjectile::DelayDestroy()
{
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AProjectile::DestroyBullet, lifeSpan, false);
}

void AProjectile::DestroyBullet()
{
	this->Destroy();
}

float AProjectile::SetDamage(float towerDamage)
{
	damage = towerDamage;
	return damage;
}


void AProjectile::SetTargetHoming(AActor* enemy)
{
	ProjectileMovement->HomingTargetComponent = enemy->GetRootComponent();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

