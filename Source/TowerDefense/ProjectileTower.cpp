// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileTower.h"
#include "Components/StaticMeshComponent.h"
#include "Enemy.h"
#include "Components/ArrowComponent.h"

AProjectileTower::AProjectileTower() 
{
	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gun Mesh"));
	GunMesh->SetupAttachment(StaticMesh);

	FirePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Fire Point"));
	FirePoint->SetupAttachment(GunMesh);
}
void AProjectileTower::Tick(float DeltaTime)
{
}

void AProjectileTower::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{

	}
}

void AProjectileTower::OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileTower::SetEnemyTarget()
{
}

void AProjectileTower::Fire()
{
}
