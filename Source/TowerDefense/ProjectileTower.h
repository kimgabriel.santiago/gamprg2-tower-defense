// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "ProjectileTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AProjectileTower : public ATowerBase
{
	GENERATED_BODY()

public:
	AProjectileTower();

	virtual void Tick(float DeltaTime) override;

protected:

	virtual void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	virtual void OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void SetEnemyTarget();

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* GunMesh;

	UPROPERTY(EditAnywhere)
	class UArrowComponent* FirePoint;


	UPROPERTY(EditAnywhere, Category = "Target Enemy")
	AActor* TargetEnemy;


	FTimerHandle TimerHandle;
	
	float FireRate;

	UFUNCTION()
	void Fire();



	

	
};
