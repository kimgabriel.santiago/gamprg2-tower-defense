// Fill out your copyright notice in the Description page of Project Settings.


#include "SalvagerTower.h"
#include "TDPlayer.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy.h"

ASalvagerTower::ASalvagerTower()
{

}

void ASalvagerTower::BeginPlay()
{
	Super::BeginPlay();

	enemyKilledGold = 100;
	playerRef = Cast<ATDPlayer>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	StartFiring();
}

void ASalvagerTower::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		if (!EnemiesInRange.Contains(enemy))
		{
			EnemiesInRange.Add(enemy);
			enemy->OnEnemyKilled.AddDynamic(this, &ASalvagerTower::OnEnemyKilled);
		}
	}
}


void ASalvagerTower::OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		if (EnemiesInRange.Contains(enemy))
		{
			EnemiesInRange.Remove(enemy);
			enemy->OnEnemyKilled.RemoveAll(this);
		}
	}
}

void ASalvagerTower::Fire()
{
	playerRef->AddCoins(passiveGold);
	isFiring = false;
}


void ASalvagerTower::OnEnemyKilled()
{
	playerRef->AddCoins(enemyKilledGold * extraGoldRate);
}

void ASalvagerTower::DoAction()
{
	Super::DoAction();

	StartFiring();

}

void ASalvagerTower::StartFiring()
{
	if (!isFiring)
	{
		GetWorldTimerManager().SetTimer(FiringTimerHandle, this, &ASalvagerTower::Fire, fireRate, true);
		isFiring = true;
	}
}
