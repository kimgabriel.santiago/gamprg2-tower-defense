// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "SalvagerTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASalvagerTower : public ATowerBase
{
	GENERATED_BODY()

public:
	ASalvagerTower();

protected:

	virtual void BeginPlay() override;

    virtual void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	virtual void Fire() override;

	virtual void DoAction() override;

	virtual void StartFiring() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ATDPlayer> BasePlayer;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float extraGoldRate;
public:

	UFUNCTION()
	void OnEnemyKilled();

	ATDPlayer* playerRef;

	int passiveGold = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int enemyKilledGold;


};
