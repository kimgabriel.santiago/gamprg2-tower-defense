// Fill out your copyright notice in the Description page of Project Settings.


#include "SingleTargetTurrets.h"
#include "Enemy.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Projectile.h"

ASingleTargetTurrets::ASingleTargetTurrets()
{
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ArrowComponent->SetupAttachment(HeadStaticMesh);

}

void ASingleTargetTurrets::BeginPlay()
{
	Super::BeginPlay();
}

void ASingleTargetTurrets::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AActor* target = Cast<AEnemy>(OtherActor))
	{
		AEnemy* tempEnemy = Cast<AEnemy>(target);

		if (!EnemiesInRange.Contains(target))
		{
			EnemiesInRange.Add(target); 
		}
		
	}
}

void ASingleTargetTurrets::OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AActor* target = Cast<AEnemy>(OtherActor))
	{
		if (target == currentEnemyTarget)
		{
			ResetTarget();
		}
		
		if (EnemiesInRange.Contains(target))
		{
			EnemiesInRange.Remove(target);
		}

	}


}

void ASingleTargetTurrets::DoAction()
{
	Super::DoAction();
	if (currentEnemyTarget == NULL)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("TargetLock"));
		ResetTarget();
		SetTarget();

	}
	else if (currentEnemyTarget)
	{
		LookTowardsTarget();
		if (isFiring == false)
		{
			StartFiring();

		}
	}
}

void ASingleTargetTurrets::Fire()
{
	Super::Fire();

	if (ProjectileRef)
	{
		AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(ProjectileRef, ArrowComponent->GetComponentTransform());
		spawnedProjectile->SetTargetHoming(currentEnemyTarget);
		spawnedProjectile->SetDamage(TowerDamage);
		isFiring = false;
	}
}

void ASingleTargetTurrets::StartFiring()
{
	if (!isFiring)
	{
		GetWorldTimerManager().SetTimer(FiringTimerHandle, this, &ASingleTargetTurrets::Fire, fireRate, true);
		isFiring = true;
	}
}

void ASingleTargetTurrets::SetTarget()
{
	if (EnemiesInRange.Num() > 0)
	{
		currentEnemyTarget = Cast<AEnemy>(EnemiesInRange[0]);
	}
}

void ASingleTargetTurrets::ResetTarget()
{
	GetWorldTimerManager().ClearTimer(FiringTimerHandle);
	currentEnemyTarget = NULL;
	isFiring = false;
}

void ASingleTargetTurrets::LookTowardsTarget()
{
	if (currentEnemyTarget != NULL)
	{
		FVector currentTargetLoc = currentEnemyTarget->StaticMesh->GetComponentLocation();

		FRotator rotator = UKismetMathLibrary::FindLookAtRotation(HeadStaticMesh->GetComponentLocation(), currentTargetLoc);

		if (isOffsetModelGun)
		{
			FRotator tempRotator = rotator;
			rotator.Pitch = tempRotator.Roll;
			rotator.Roll = tempRotator.Pitch;

			rotator.Yaw -= 90;
		}

		//FVector forward = currentEnemyTarget->ActorToWorld().GetLocation() - HeadStaticMesh->GetComponentTransform().GetLocation();
		//FRotator rot = UKismetMathLibrary::MakeRotFromXZ(forward, FVector::UpVector);
		HeadStaticMesh->SetWorldRotation(rotator);
	}
}

void ASingleTargetTurrets::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DoAction();
}

