// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "SingleTargetTurrets.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASingleTargetTurrets : public ATowerBase
{
	GENERATED_BODY()
public:

	ASingleTargetTurrets();

protected:
	virtual void BeginPlay() override;

	virtual void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	virtual void DoAction() override;

	virtual void Fire() override;

	virtual void StartFiring() override;

	virtual void SetTarget();

	UFUNCTION()
	void ResetTarget();

	UFUNCTION()
	void LookTowardsTarget();


	UPROPERTY(VisibleAnywhere, Category = "Target")
	class AEnemy* currentEnemyTarget;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UArrowComponent* ArrowComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isOffsetModelGun;

public:
	virtual void Tick(float DeltaTime) override;

};
