// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowBuff.h"
#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"

ASlowBuff::ASlowBuff()
{

}

void ASlowBuff::BuffEffectStart()
{
	float enemySpeed = Cast<AEnemy>(target)->baseSpeed;
	float newSpeed = enemySpeed * buffPow;

	Cast<AEnemy>(target)->ModifySpeed(newSpeed);

}

void ASlowBuff::BuffEffectEnd()
{
	float enemySpeed = Cast<AEnemy>(target)->GetCharacterMovement()->GetMaxSpeed();
	float newSpeed = enemySpeed / buffPow;

	Cast<AEnemy>(target)->ModifySpeed(newSpeed);
}
