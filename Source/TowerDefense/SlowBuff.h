// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "SlowBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASlowBuff : public ABuff
{
	GENERATED_BODY()
public:
	ASlowBuff();
protected:

	void BuffEffectStart() override;

	void BuffEffectEnd() override;
	
};
