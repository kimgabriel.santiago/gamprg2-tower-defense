// Fill out your copyright notice in the Description page of Project Settings.


#include "TDPlayer.h"
#include "HealthComponent.h"


// Sets default values
ATDPlayer::ATDPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	this->AddOwnedComponent(HealthComponent);
}

// Called when the game starts or when spawned
void ATDPlayer::BeginPlay()
{
	Super::BeginPlay();
	SpawnDefaultController();
}

// Called every frame
void ATDPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


int32 ATDPlayer::ReduceHealth(int damage)
{
	return int32();
}

int32 ATDPlayer::GetCoins()
{
	return coins;
}

int32 ATDPlayer::AddCoins(int coinsToAdd)
{
	coins += coinsToAdd;
	return coins;
}

int32 ATDPlayer::CoinsReduce(int coinsToDeduct)
{
	coins -= coinsToDeduct;
	return coins;
}

void ATDPlayer::CheckDead()
{
	if (HealthComponent->GetCurrentHP() <= 0)
	{
		OnPlayerDead.Broadcast();
	}
}

