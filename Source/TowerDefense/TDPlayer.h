// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TDPlayer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerDeadSignature);

UCLASS()
class TOWERDEFENSE_API ATDPlayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATDPlayer();

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
	FPlayerDeadSignature OnPlayerDead;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere)
	int32 coins;

	
public:	
	// Called every frame

	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	int32 ReduceHealth(int damage);

	UFUNCTION(BlueprintCallable)
	int32 GetCoins();

	UFUNCTION(BlueprintCallable)
	int32 AddCoins(int coinsToAdd);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly);
	class UHealthComponent* HealthComponent;

	UFUNCTION(BlueprintCallable)
	int32 CoinsReduce(int coinsToDeduct);

	UFUNCTION()
	void CheckDead();



};
