// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerBase.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Enemy.h"

// Sets default values
ATowerBase::ATowerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	BaseStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	RootComponent = BaseStaticMesh;

	HeadStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head Mesh"));
	HeadStaticMesh->SetupAttachment(RootComponent);

	SphereRange = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Range"));
	SphereRange->SetupAttachment(RootComponent);

	isFiring = false;
	fireRate = 9999;

}

// Called when the game starts or when spawned
void ATowerBase::BeginPlay()
{
	Super::BeginPlay();
	SphereRange->OnComponentBeginOverlap.AddDynamic(this, &ATowerBase::OnEnemyBeginOverlap);
	SphereRange->OnComponentEndOverlap.AddDynamic(this, &ATowerBase::OnEnemyOverlapEnd);
	
}

void ATowerBase::OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
}

void ATowerBase::OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}


void ATowerBase::Fire()
{
	/*GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Fired"));*/
}

void ATowerBase::DoAction()
{
	/*GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Do Action"));*/
}

void ATowerBase::StartFiring()
{
	
}

void ATowerBase::CalculateTarget()
{
	float dist = 99999999.0f;
	float currentDist;

	if (EnemiesInRange.Num() > 0)
	{
		for (AActor* enemy : EnemiesInRange)
		{
			currentDist = FVector::Distance(this->GetActorLocation(), enemy->GetActorLocation());
			if (currentDist < dist)
			{
				TargetEnemy = enemy;
				dist = currentDist;
			}
		}
	}
	else if (EnemiesInRange.Num() <= 0)
	{
		TargetEnemy = nullptr;
	}
}

// Called every frame
void ATowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

