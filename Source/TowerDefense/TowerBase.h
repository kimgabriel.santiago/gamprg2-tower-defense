// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerBase.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Components")
	class UStaticMeshComponent* BaseStaticMesh;

	UPROPERTY(EditAnywhere, Category = "Components")
	class USphereComponent* SphereRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* HeadStaticMesh;
	
	UPROPERTY(EditAnywhere)
	TArray<AActor*> EnemiesInRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class AProjectile> ProjectileRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float radius;

	UPROPERTY(VisibleAnywhere)
	FTimerHandle FiringTimerHandle;
	
	UPROPERTY(EditAnywhere, Category = "Stats")
	float fireRate;

	  
	UFUNCTION()
	virtual void OnEnemyBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void OnEnemyOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	virtual void Fire();

	UFUNCTION()
	virtual void DoAction();

	UFUNCTION()
	virtual void StartFiring();

	void CalculateTarget();


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Stats")
	bool isFiring;

	UPROPERTY(EditAnywhere)
	float TowerDamage;

	UPROPERTY(EditAnywhere, Category = "Target Enemy")
	AActor* TargetEnemy;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int cost;
};
