// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ATowerBase> Tower;
	
	UPROPERTY(EditAnywhere)
	int cost;

	UPROPERTY(EditAnywhere)
	int damage;

	UPROPERTY(EditAnywhere)
	int level;

};
