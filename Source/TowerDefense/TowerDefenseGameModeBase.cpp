// Copyright Epic Games, Inc. All Rights Reserved.


#include "TowerDefenseGameModeBase.h"
#include "WaveData.h"
#include "Enemy.h"
#include "WaveSpawner.h"
#include "Kismet/GameplayStatics.h"
#include "TDPlayer.h"
#include "HealthComponent.h"

ATowerDefenseGameModeBase::ATowerDefenseGameModeBase()
{
	WaveCount = 1;
	CurrentWave = 0;

}

void ATowerDefenseGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	ATDPlayer* player = Cast<ATDPlayer>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	player->OnPlayerDead.AddDynamic(this, &ATowerDefenseGameModeBase::RestartGameLevel);
}

void ATowerDefenseGameModeBase::Tick(float DeltaTime)
{
	
}

void ATowerDefenseGameModeBase::StartWave(TArray<class AWaveSpawner*> spawnersRef)
{

	WaveSpawnMax = WaveDataArray[CurrentWave]->NumSpawns;

	for (int i = 0; i < SpawnPoints.Num(); i++)
	{
		SpawnPoints[i]->WaveSetup(WaveDataArray[CurrentWave]->Monsters,WaveDataArray[CurrentWave]->NumSpawns, WaveCount);
		SpawnPoints[i]->SpawnEnemies(); 
	}

	WaveSpawnMax = WaveDataArray[CurrentWave]->NumSpawns * SpawnPoints.Num();

	CurrentWave++;
	
}

void ATowerDefenseGameModeBase::OnEnemyKilled()
{
	WaveSpawnKilled++;

	if (WaveSpawnKilled >= WaveSpawnMax)
	{
		WaveSpawnKilled = 0;
		WaveCount++;

		ATDPlayer* player = Cast<ATDPlayer>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		player->AddCoins(WaveDataArray[CurrentWave]->ClearReward);

		StartWave(SpawnPoints);
	}
}

void ATowerDefenseGameModeBase::OnEnemyDeath()
{
	ATDPlayer* player = Cast<ATDPlayer>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	player->AddCoins(WaveDataArray[CurrentWave]->KillReward);
	
}

void ATowerDefenseGameModeBase::RestartGameLevel()
{
	
}

int32 ATowerDefenseGameModeBase::GetWaveKillCount()
{
	return WaveSpawnKilled;
}

int32 ATowerDefenseGameModeBase::GetWaveSpawnMax()
{
	return WaveSpawnMax;
}


