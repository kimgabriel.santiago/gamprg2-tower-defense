// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATowerDefenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	ATowerDefenseGameModeBase();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class AWaveSpawner*> SpawnPoints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 WaveCount;

	int32 CurrentWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 WaveSpawnMax;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 WaveSpawnKilled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class UWaveData*> WaveDataArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class AEnemy*> EnemySpawned;

	TArray<class ABaseCore*> Cores;

	float elapsedTime;

	float waitingCount = 10.0f;
public:

	virtual void Tick(float DeltaTime) override;

	FTimerHandle WaveTimer;

	UFUNCTION(BlueprintCallable)
	void StartWave(TArray<class AWaveSpawner*> spawnersRef);

	UFUNCTION()
	void OnEnemyKilled();

	UFUNCTION()
	void OnEnemyDeath();

	UFUNCTION()
	void RestartGameLevel();

	UFUNCTION(BlueprintCallable)
	int32 GetWaveKillCount();

	UFUNCTION(BlueprintCallable)
	int32 GetWaveSpawnMax();
	

	
};
