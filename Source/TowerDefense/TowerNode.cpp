// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerNode.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"

// Sets default values
ATowerNode::ATowerNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = SceneComponent;

	NodeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Node Mesh"));
	NodeMesh->SetupAttachment(SceneComponent);
	NodeMesh->OnBeginCursorOver.AddDynamic(this, &ATowerNode::OnBeginMouseOver);
	NodeMesh->OnEndCursorOver.AddDynamic(this, &ATowerNode::OnEndMouseOver);
	NodeMesh->OnClicked.AddDynamic(this, &ATowerNode::OnMouseClicked);
	NodeMesh->OnReleased.AddDynamic(this, &ATowerNode::OnMouseReleased);

	TowerSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Tower Spawn Point"));
	TowerSpawnPoint->SetupAttachment(RootComponent);


}

// Called when the game starts or when spawned
void ATowerNode::BeginPlay()
{
	Super::BeginPlay();
	canBuildTower = true;
}

void ATowerNode::OnBeginMouseOver(UPrimitiveComponent* TouchedComponent)
{
	if (canBuildTower == true)
	{
		TouchedComponent->SetMaterial(0, BuildableMaterial);
	}
	else
	{

		TouchedComponent->SetMaterial(0, NotBuildableMaterial);
	}
}

void ATowerNode::OnEndMouseOver(UPrimitiveComponent* TouchedComponent)
{
	TouchedComponent->SetMaterial(0, InitialTowerNodeMaterial);
	
}

void ATowerNode::OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	if (canBuildTower == true)
	{
		OnClickedEvent(this);
	}
}

void ATowerNode::OnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
}



// Called every frame
void ATowerNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

