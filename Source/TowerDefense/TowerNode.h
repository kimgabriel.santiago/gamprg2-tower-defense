// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerNode.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerNode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* NodeMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UArrowComponent* TowerSpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMaterial* BuildableMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMaterial* InitialTowerNodeMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMaterial* NotBuildableMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool canBuildTower;

	UFUNCTION()
	void OnBeginMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void OnEndMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UFUNCTION()
	void OnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UFUNCTION(BlueprintImplementableEvent)
	void OnClickedEvent(ATowerNode* TowerNode);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
