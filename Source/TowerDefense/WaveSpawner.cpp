// Fill out your copyright notice in the Description page of Project Settings.


#include "WaveSpawner.h"
#include "Enemy.h"
#include "Engine/DataTable.h"
#include "TowerDefenseGameModeBase.h"


// Sets default values
AWaveSpawner::AWaveSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AWaveSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWaveSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWaveSpawner::SpawnEnemies()
{
	if (spawnItr < currentWaveSpawnCount) 
	{
		FActorSpawnParameters spawnParams;

		AEnemy* newEnemy = GetWorld()->SpawnActor<AEnemy>(currentWaveSpawn[0], GetActorLocation(), GetActorRotation(), spawnParams);
		newEnemy->ScaleHealth(waveCount);
		newEnemy->SetWaypoints(waypoints);
		newEnemy->MoveNextWaypoint();

		ATowerDefenseGameModeBase* TWDGameMode = GetWorld()->GetAuthGameMode<ATowerDefenseGameModeBase>();
		newEnemy->OnEnemyDeath.AddDynamic(TWDGameMode, &ATowerDefenseGameModeBase::OnEnemyKilled);
		newEnemy->OnEnemyKilled.AddDynamic(TWDGameMode, &ATowerDefenseGameModeBase::OnEnemyDeath);
		spawnItr++;


		GetWorldTimerManager().SetTimer(spawnTimer, this, &AWaveSpawner::SpawnEnemies, timeDelay, false);
	}
	else
	{
		spawnItr = 0;
	}


}

void AWaveSpawner::WaveSetup(TArray<TSubclassOf<class ACharacter>> Enemy, int32 spawnCount, int currentWave)
{
	currentWaveSpawn = Enemy;
	currentWaveSpawnCount = spawnCount;
	
	GetWorldTimerManager().SetTimer(spawnTimer, this, &AWaveSpawner::SpawnEnemies, timeDelay, false);

}




 