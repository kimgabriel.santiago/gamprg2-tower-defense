// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WaveSpawner.generated.h"


UCLASS()
class TOWERDEFENSE_API AWaveSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWaveSpawner();



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 currentWaveSpawnCount;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor*> waypoints;

	TArray<TSubclassOf<class ACharacter>> currentWaveSpawn;
	FTimerHandle spawnTimer;

	int32 waveCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float timeDelay = 3.0f;

	int spawnItr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float difficultyScale; 
 
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SpawnEnemies();

	UFUNCTION(BlueprintCallable)
	void WaveSetup(TArray<TSubclassOf<class ACharacter>> Enemy, int32 spawnCount, int currentWave);


};
